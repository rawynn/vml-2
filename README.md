# Symfony Installation Powered By Docksal

This is a sample application installation pre-configured for use with Docksal.  


## Setup instructions

### Step #1: Docksal environment setup

**This is a one time setup - skip this if you already have a working Docksal environment.**  

Follow [Docksal environment setup instructions](https://docs.docksal.io/en/master/getting-started/setup)

### Step #2: Project setup

1. Clone this repo into your Projects directory

        git clone https://bitbucket.org/Flower7C3/demo-symfony-4-crud.git symfony-crud-demo
        cd symfony-crud-demo

2. Initialize the site

    This will initialize local settings and install the site connection file.

        fin init

3. Point your browser to [http://symfony-crud-demo.docksal](http://symfony-crud-demo.docksal)



## Testing

1. With PHPUnit from _phpunit.xml.dist_ file:

        fin phpunit
    
2. With bash from _.docksal/commands/test_ file:

        fin test



## Other instructions

If You want to recreate database just run `fin init-site`. To start app use `fin start` and stop with `fin stop`.
To remove project from Docksal run `fin rm`. Read more about Docksal Symfony Skeleton on [Github](https://github.com/docksal/example-symfony-skeleton).

