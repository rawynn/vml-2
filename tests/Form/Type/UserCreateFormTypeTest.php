<?php

namespace App\Tests\Form\Type;

use App\Entity\User;
use App\Form\Type\ArticleCreateFormType;
use Symfony\Component\Form\Test\TypeTestCase;

class UserCreateFormTypeTest extends TypeTestCase
{

    public function testSubmitValidData()
    {
        $formData = array(
            'name' => 'Jan',
            'surname' => 'Kowalsky',
            'phone' => '123456789',
            'address' => 'Krakow',
        );

        $objectToCompare = new User();
        # $objectToCompare will retrieve data from the form submission; pass it as the second argument
        $form = $this->factory->create(ArticleCreateFormType::class, $objectToCompare);

        # submit the data to the form directly
        $form->submit($formData);
        $this->assertTrue($form->isSynchronized());

        # ...populate $object properties with the data stored in $formData
        $object = new User();
        $object
            ->setName($formData['name'])
            ->setSurname($formData['surname'])
            ->setPhone($formData['phone'])
            ->setAddress($formData['address']);

        # check that $objectToCompare was modified as expected when the form was submitted
        $this->assertEquals($object, $objectToCompare);

        $view = $form->createView();
        $children = $view->children;
        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }

}
