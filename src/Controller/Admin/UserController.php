<?php

namespace App\Controller\Admin;

use App\Controller\Core\AbstractDefaultController;
use App\DomainManager\UserDomainManager;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller
 * @Route("/dashboard/user")
 * @property UserDomainManager $domainManager
 */
class UserController extends AbstractDefaultController
{
    /**
     * @Route("/list.html", methods={Request::METHOD_GET})
     * @param Request $request
     * @return Response
     */
    public function list(Request $request)
    {
        $items = $this->domainManager->getItems($request);
        return $this->render('admin/user/list.html.twig', [
            'items' => $items,
        ]);
    }

    /**
     * @Route("create.html", methods={Request::METHOD_GET,Request::METHOD_POST})
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        try {
            $form = $this->domainManager->createActionBuildForm();
            if ($this->domainManager->createActionHandleRequest($form, $request)) {
                $this->addFlash(
                    'success',
                    'User successfully created'
                );
                return $this->redirectToRoute('app_admin_article_list');
            }
            return $this->render('admin/user/create.html.twig', [
                'form' => $form->createView(),
            ]);
        } catch (\Exception $exception) {
            throw new ServiceUnavailableHttpException(null, (string)$exception->getMessage());
        }
    }

    /**
     * @Route("/{id}/read.html", methods={Request::METHOD_GET})
     * @return Response
     */
    public function read(Request $request)
    {
        try {
            $item = $this->domainManager->getItem($request);
            return $this->render('admin/user/read.html.twig', [
                'item' => $item,
            ]);
        } catch (EntityNotFoundException $exception) {
            throw new NotFoundHttpException('User not found!');
        } catch (\Exception $exception) {
            throw new ServiceUnavailableHttpException(null, (string)$exception->getMessage());
        }
    }

    /**
     * @Route("/{id}/update.html", methods={Request::METHOD_GET,Request::METHOD_PUT})
     * @return Response
     */
    public function update(Request $request)
    {
        try {
            $form = $this->domainManager->updateActionBuildForm($request);
            if ($this->domainManager->updateActionHandleRequest($form, $request)) {
                $this->addFlash(
                    'success',
                    'User successfully updated'
                );
                return $this->redirectToRoute('app_admin_article_list');
            }
            return $this->render('admin/user/update.html.twig', [
                'form' => $form->createView(),
                'item' => $form->getData(),
            ]);
        } catch (EntityNotFoundException $exception) {
            throw new NotFoundHttpException('User not found!');
        } catch (\Exception $exception) {
            throw new ServiceUnavailableHttpException(null, (string)$exception->getMessage());
        }
    }

    /**
     * @Route("/{id}/delete.html", methods={Request::METHOD_GET,Request::METHOD_DELETE})
     * @return Response
     */
    public function delete(Request $request)
    {
        try {
            $form = $this->domainManager->deleteActionBuildForm($request);
            if ($this->domainManager->deleteActionHandleRequest($form, $request)) {
                $this->addFlash(
                    'success',
                    'User successfully deleted'
                );
                return $this->redirectToRoute('app_admin_article_list');
            }
            return $this->render('admin/user/delete.html.twig', [
                'form' => $form->createView(),
                'item' => $form->getData(),
            ]);
        } catch (EntityNotFoundException $exception) {
            throw new NotFoundHttpException('User not found!');
        } catch (\Exception $exception) {
            throw new ServiceUnavailableHttpException(null, (string)$exception->getMessage());
        }
    }
}
