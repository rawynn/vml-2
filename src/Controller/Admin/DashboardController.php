<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DashboardController
 * @package App\Controller
 * @Route("/dashboard")
 */
class DashboardController extends AbstractController
{
    /**
     * @Route("/index", name="app_admin_index")
     * @return Response
     */
    public function index()
    {
        return $this->render('admin/index.html.twig', []);
    }

}
