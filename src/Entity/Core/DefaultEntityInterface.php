<?php

namespace App\Entity\Core;

interface DefaultEntityInterface
{
    public function getId();
}
