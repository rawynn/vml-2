<?php

namespace App\Repository\Core;

use Doctrine\ORM\EntityRepository;

abstract class AbstractDefaultEntityRepository extends EntityRepository implements DefaultEntityRepositoryInterface
{

}
