<?php

namespace App\DomainManager;

use App\DomainManager\Core\AbstractDefaultDomainManager;
use App\Entity\Core\DefaultEntityInterface;
use App\Entity\Article;
use App\Form\Type\ArticleCreateFormType;
use App\Form\Type\ArticleDeleteFormType;
use App\Form\Type\ArticleUpdateFormType;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ArticleDomainManager
 * @package App\Manager
 */
class ArticleDomainManager extends AbstractDefaultDomainManager
{

    public function getItems(Request $request): array
    {
        return $this->entityManager->getRepository('App:Article')->findAll();
    }

    public function getLatestItems(Request $request): array
    {
        return $this->entityManager->getRepository('App:Article')->findBy(array(),array("date" => "DESC"));
    }  
    /**
     * @param Request $request
     * @return DefaultEntityInterface
     * @throws EntityNotFoundException
     */
    public function getItem(Request $request): DefaultEntityInterface
    {
        $article = $this->entityManager->getRepository('App:Article')->find($request->get('id'));
        if (!$article instanceof Article) {
            throw new EntityNotFoundException();
        }
        return $article;
    }

    public function createActionBuildForm(): FormInterface
    {
        $article = new Article();
        return $this->formHandler->createForm(ArticleCreateFormType::class, $article);
    }

    public function createActionHandleRequest(FormInterface $form, Request $request): bool
    {
        if ($this->formHandler->handleRequest($form, $request, Request::METHOD_POST)) {
            /** @var Article $article */
            $article = $form->getData();
            $this->entityManager->persist($article);
            $this->entityManager->flush();
            return true;
        }
        return false;
    }

    public function updateActionBuildForm(Request $request): FormInterface
    {
        $article = $this->getItem($request);
        return $this->formHandler->createForm(ArticleUpdateFormType::class, $article);
    }

    public function updateActionHandleRequest(FormInterface $form, Request $request): bool
    {
        if ($this->formHandler->handleRequest($form, $request, Request::METHOD_PUT)) {
            /** @var Article $article */
            $article = $form->getData();
            $this->entityManager->persist($article);
            $this->entityManager->flush();
            return true;
        }
        return false;
    }

    public function deleteActionBuildForm(Request $request): FormInterface
    {
        $article = $this->getItem($request);
        return $this->formHandler->createForm(ArticleDeleteFormType::class, $article);
    }

    public function deleteActionHandleRequest(FormInterface $form, Request $request): bool
    {
        if ($this->formHandler->handleRequest($form, $request, Request::METHOD_DELETE)) {
            /** @var Article $article */
            $article = $form->getData();
            $this->entityManager->remove($article);
            $this->entityManager->flush();
            return true;
        }
        return false;
    }

  
}
