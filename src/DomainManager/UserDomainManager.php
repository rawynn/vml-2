<?php

namespace App\DomainManager;

use App\DomainManager\Core\AbstractDefaultDomainManager;
use App\Entity\Core\DefaultEntityInterface;
use App\Entity\User;
use App\Form\Type\ArticleCreateFormType;
use App\Form\Type\ArticleDeleteFormType;
use App\Form\Type\UserUpdateFormType;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserDomainManager
 * @package App\Manager
 */
class UserDomainManager extends AbstractDefaultDomainManager
{

    public function getItems(Request $request): array
    {
        return $this->entityManager->getRepository('App:User')->findAll();
    }

    /**
     * @param Request $request
     * @return DefaultEntityInterface
     * @throws EntityNotFoundException
     */
    public function getItem(Request $request): DefaultEntityInterface
    {
        $user = $this->entityManager->getRepository('App:User')->find($request->get('id'));
        if (!$user instanceof User) {
            throw new EntityNotFoundException();
        }
        return $user;
    }

    public function createActionBuildForm(): FormInterface
    {
        $user = new User();
        return $this->formHandler->createForm(ArticleCreateFormType::class, $user);
    }

    public function createActionHandleRequest(FormInterface $form, Request $request): bool
    {
        if ($this->formHandler->handleRequest($form, $request, Request::METHOD_POST)) {
            /** @var User $user */
            $user = $form->getData();
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            return true;
        }
        return false;
    }

    public function updateActionBuildForm(Request $request): FormInterface
    {
        $user = $this->getItem($request);
        return $this->formHandler->createForm(UserUpdateFormType::class, $user);
    }

    public function updateActionHandleRequest(FormInterface $form, Request $request): bool
    {
        if ($this->formHandler->handleRequest($form, $request, Request::METHOD_PUT)) {
            /** @var User $user */
            $user = $form->getData();
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            return true;
        }
        return false;
    }

    public function deleteActionBuildForm(Request $request): FormInterface
    {
        $user = $this->getItem($request);
        return $this->formHandler->createForm(ArticleDeleteFormType::class, $user);
    }

    public function deleteActionHandleRequest(FormInterface $form, Request $request): bool
    {
        if ($this->formHandler->handleRequest($form, $request, Request::METHOD_DELETE)) {
            /** @var User $user */
            $user = $form->getData();
            $this->entityManager->remove($user);
            $this->entityManager->flush();
            return true;
        }
        return false;
    }
}
