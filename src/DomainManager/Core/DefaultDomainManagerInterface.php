<?php

namespace App\DomainManager\Core;

use App\Entity\Core\DefaultEntityInterface;
use Symfony\Component\HttpFoundation\Request;

interface DefaultDomainManagerInterface
{
    public function getItems(Request $request): array;

    public function getItem(Request $request): DefaultEntityInterface;

   // public function getNewestItems(Request $request): array;
}
