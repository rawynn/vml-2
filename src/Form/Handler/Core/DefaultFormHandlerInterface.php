<?php

namespace App\Form\Handler\Core;

use App\Entity\Core\DefaultEntityInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

interface DefaultFormHandlerInterface
{
    public function createForm($formName, DefaultEntityInterface $entity):FormInterface;

    public function handleRequest(FormInterface $form, Request $request): bool;
}
